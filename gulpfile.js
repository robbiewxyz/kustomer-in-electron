const gulp = require('gulp');
const ts = require('gulp-typescript');
const replace = require('gulp-replace');
const clean = require('gulp-clean');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('clean-build', function() {
    return gulp.src('./build', { read: false, allowEmpty: true }).pipe(clean());
});

gulp.task('directories', function() {
    return gulp
        .src('*.*', { read: false })
        .pipe(gulp.dest('./build'))
        .pipe(gulp.dest('./build/gui'))
        .pipe(gulp.dest('./build/gui/bin'));
});

gulp.task('window-tsc', function(cb) {
    const tsProject = ts.createProject('tsconfig.window.json');
    return tsProject
        .src()
        .pipe(tsProject())
        .js.pipe(gulp.dest('./'));
});

gulp.task('main-tsc', function(cb) {
    const tsProject = ts.createProject('tsconfig.main.json');
    return tsProject
        .src()
        .pipe(sourcemaps.init())
        .pipe(tsProject())
        .js.pipe(sourcemaps.write())
        .pipe(gulp.dest('build'));
});

gulp.task('copy-html', function() {
    return gulp.src(['./src/gui/scenes/*.html']).pipe(gulp.dest('build/gui/scenes'));
});

gulp.task('default', gulp.series('clean-build', 'directories', gulp.parallel('main-tsc', 'window-tsc', 'copy-html')));
