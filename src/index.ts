import { app } from 'electron';
import { StatusWindowManager } from './common/StatusWindowManager';

app.once('ready', () => {
    StatusWindowManager.getInstance();
});
